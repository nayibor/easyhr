<?php

/*
  //shortcut for  login
  //have to also work on the site and hte institution lock --later
 * //may have to add some capthca to the login page --later
 */

class DashboardsController extends AppController {

    public $helpers = array('Html', 'Form');
    public $components = array('RequestHandler', 'Session', 'Cookie');
    public $paginate = array('limit' => 10);
    public $uses = array('Vwpayslip','User');
    public $layout = "dashboard";

   
    function beforeFilter() {
        $this->checkMainCred();
    }

    //this used for checking the main page to see whether  u have to be redirected to  reception
    function checkMainCred() {


        if ($this->Session->check('memberData') && $this->action != 'logoutUser') {
            $this->redirect(array('controller' => 'receptions', 'action' => 'index'));
        }
    }

    //shortcut for  logout
    function logoutUser() {
        $this->Session->delete('memberData');
        $this->Session->delete('site_id');
        $this->Session->delete('inst_id');
        $this->redirect(array('controller' => 'dashboards', 'action' => 'index'));
    }

    function loginUser($user) {

        $this->Session->write('memberData', $user);
        $this->Session->write('site_id', $user['User']['site_id']);
        //  $this->getLinks($user['User']['id']);
        $this->redirect(array('controller' => 'receptions', 'action' => 'index'));
    }

    //shortcut for checking login for users at login page
    function index() {
        if (isset($_POST['username']) && isset($_POST['password'])
                && $_POST['username'] != "" && $_POST['password'] != ""
        ) {
            $users = $this->User->checkUser($_POST['username'], $_POST['password']);
            //user account username and pass is available but account may be locked
            if (isset($users['User'])) {
                //   print_r($users);
                //     exit();
                //will have to check for a  site lock as well as an institution lock here

                if ($users['User']['lock_status'] >= 3) {
                    $user_login = "false";
                    $msg = "Account Is Locked .Please Contact Administrator";
                    $this->set(compact('user_login', 'msg'));
                } else if ($users['User']['lock_status'] < 3) {
                    //login user
                    //
     
                    $this->User->set(array('id' => $users['User']['id'], 'lock_status' => '0'));
                    $this->User->save();
                    $user_login = "false";
                    //  $msg = "User Successfull Logged In";
                    //   $this->set(compact('user_login', 'msg'));
                    $this->loginUser($users);
                }
            } else {
                //user wasnt able to login but other stuff is being tested 
                $user = $this->User->find('first', array("conditions" => array("User.user_email" => $_POST['username'])));
                if (isset($user) && sizeof($user) > 1 && $user['User']['lock_status'] < 3) {
                    // echo "user-statushere" . $user['User']['lock_status'];
                    $new_lock_number = intval($user['User']['lock_status']) + 1;
                    $usern = new User();
                    $usern->set(array(
                        'id' => $user['User']['id'],
                        'lock_status' => $new_lock_number
                    ));
                    $usern->save();
                    $user_login = "false";
                    $msg = "Please Enter Correct Username and Password To Login";
                } else if (isset($user) && sizeof($user) > 1 && $user['User']['lock_status'] >= 3) {
                    $user_login = "false";
                    $msg = "Account Is Locked .Please Contact Administrator ";
                } else {
                    $user_login = "false";
                    $msg = "Please Enter Correct Username and Password To Login";
                }
                $this->set(compact('user_login', 'msg'));
            }
        } else {
            $user_login = "false";
            $msg = "Please sign in to Easy HR Payroll Dashboard";
            $this->set(compact('user_login', 'msg'));
        }
    }

}

?>
