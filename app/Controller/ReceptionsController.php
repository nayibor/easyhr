<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *testing to see how this editor works out after all
 */
 
App::uses('AppController', 'Controller');

class ReceptionsController extends AppController {


    public $helpers = array('Html','Form');
    public $components = array('RequestHandler', 'Session', 'Cookie','Pdf');
    public $paginate = array('limit' => 10);
    public $uses = array('Vwpayslip','User');
    public $layout = "reception";



    function beforeFilter() {

      parent::beforeFilter();
    }

    //this is the default page that users will log into when they are entering  the system 
    function index() {
    
    
    }
   ///function has to be added to check old password 
  //also to compare repeated password to see if staff memeber got the reset correct
    function change_password() {
        if (isset($_POST['pass_data'])) {
            $this->autoRender = false;
            $pass = $_POST['pass_data'];
            $member_data = $this->Session->read('memberData');
            // print_r($member_data);
            //  echo $pass . "------" . hash('sha256', $pass);
            //   exit;
            if ($pass != "") {
                $usern = new User();
                $usern->set(array(
                    'id' => $member_data['User']['id'],
                    'password' => trim(hash('sha256', $pass))
                ));
                $usern->save();
                echo json_encode(array("status" => "true", "msg" => "passord change successfull"));
            } else if ($pass == "") {
                echo json_encode(array("status" => "false", "msg" => "password is empty"));
            }
        }
        return;
    }

    function payroll_list($paginate_link = null) {

        $this->autoLayout = false;
        $member_data = $this->Session->read('memberData');
        $conditions_array = array('Vwpayslip.StaffNo' => $member_data['User']['user_email']);
        $filter = isset($_GET['filter']) && $_GET['filter'] != "" ? $_GET['filter'] : "";
        $sfilter = isset($_GET['sfilter']) && $_GET['sfilter'] != "" ? $_GET['sfilter'] : "";

        if ($filter != "null" && $sfilter != "null") {

            $newfdate = explode('/', $filter);
            $newsdate = explode('/', $sfilter);
            //  print_r($newfdate);
            //   print_r($newsdate);
            $newfd = $newfdate[2] . '-' . $newfdate[1] . '-' . $newfdate[0];
            $newsd = $newsdate[2] . '-' . $newsdate[1] . '-' . $newsdate[0];

            $conditions_array['AND'] = array(
                'Vwpayslip.PayPeriod >=' => $newfd,
                'Vwpayslip.PayPeriod <=' => $newsd
            );
        };

        if ($paginate_link != null) {

            $page_array = explode($paginate_link, ":");
            $this->paginate = array(
                'Vwpayslip' => array(
                    'conditions' => $conditions_array,
                    'order' => array('Vwpayslip.PayPeriod' => 'asc'),
                    'group' => array('Vwpayslip.PayPeriod'),
                    'page' => $page_array[1],
                    'limit' => 10));


            $slips = $this->paginate('Vwpayslip');
        } else {
            $this->paginate = array(
                'Vwpayslip' => array(
                    'conditions' => $conditions_array,
                    'order' => array('Vwpayslip.PayPeriod' => 'asc'),
                    'group' => array('Vwpayslip.PayPeriod'),
                    'limit' => 10));
            $slips = $this->paginate('Vwpayslip');
        }


        $this->set(compact('slips'));
    }

    function index_new() {
        
    }

    function view_payslip() {
        $this->autoLayout = false;
        $filter = isset($_GET['payperiod']) && $_GET['payperiod'] != "" ? $_GET['payperiod'] : "";
        $member_data = $this->Session->read('memberData');
        $conditions_array = array('Vwpayslip.StaffNo' => $member_data['User']['user_email']);
        $conditions_array[] = array('Vwpayslip.PayPeriod' => $filter);

        $payslips = $this->Vwpayslip->find("all", array("conditions" => $conditions_array));
        //print_r($payslips);
        // exit;
        $this->set(compact('payslips'));
    }

    function download_payslip() {
        $this->autoLayout = false;
        $filter = isset($_GET['payperiod']) && $_GET['payperiod'] != "" ? $_GET['payperiod'] : "";
        $member_data = $this->Session->read('memberData');
        $conditions_array = array('Vwpayslip.StaffNo' => $member_data['User']['user_email']);
        $conditions_array[] = array('Vwpayslip.PayPeriod' => $filter);
        $payslips = $this->Vwpayslip->find("all", array("conditions" => $conditions_array));
       // print_r($payslips);
      //  exit();
        $name = date('M Y', strtotime($payslips[0]['Vwpayslip']['PayPeriod']));
        $this->Pdf->download("Payslip_{$name}.pdf", '/elements/view_payslip_element', array('payslips' => $payslips));
        exit();
    }


}

?>
