<?php
/*
 * To change this template, choose Tools | Templates
 */

?>



<form id="loginform" method="post" action="<?php echo $this->Html->url(array('controller' => 'dashboards', 'action' => 'index')); ?>">
    <div class="alert alert-warning no-radius no-margin padding-sm"><i class="fa fa-info-circle"></i> <?php echo $msg ?></div>
    <div class="box-body padding-md">
        <div class="form-group">
            <input type="text" name="username" class="form-control" placeholder="Employee Number"/>
        </div>
        <div class="form-group">
            <input type="password" name="password" class="form-control" placeholder="Password"/>
        </div>
        <div class="box-footer">                                                               
            <button type="submit" class="btn btn-success btn-block">Sign in</button>  
        </div>
    </div>
</form>