<style> h3 {
        font-size: 15px;
        font-weight: bold;
        border-bottom: dotted #ccc 1px;
        margin :10px;
        color: #222;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
    .summary_table tr.higher_order, table.higher_order tr td:first-child{
    font-size: 11.5px;
    font-family: Tahoma, Verdana, Arial, sans-serif;font-weight: bold;
    cellpadding:6px;
    }

 .summary_table tr{
    font-size: 11.5px;
    font-family: Tahoma, Verdana, Arial, sans-serif;
    cellpadding:6px;
    }

</style>

<div>
    <h3>Pay Advice Slip</h2>

</div>
<div style="margin: 15px;">
    <table  class="summary_table higher_order"  width="50%">
        <tbody><tr>
                <td>Name</td>
                <td><?php echo $payslips[0]['Vwpayslip']['Name']; ?></td>
            </tr>

            <tr>
                <td>Department</td>
                <td><?php echo $payslips[0]['Vwpayslip']['Department']; ?></td>
            </tr>

            <tr>
                <td>Rank</td>
                <td><?php echo $payslips[0]['Vwpayslip']['RANK']; ?></td>
            </tr>

            <tr>
                <td>Business Unit</td>
                <td><?php echo $payslips[0]['Vwpayslip']['BusinessUnit']; ?></td>
            </tr>

            <tr>
                <td>Category </td>
                <td><?php echo $payslips[0]['Vwpayslip']['Category']; ?></td>
            </tr>

            <tr>
                <td>Staff No</td>
                <td><?php echo $payslips[0]['Vwpayslip']['StaffNo']; ?></td>
            </tr>

            <tr>
                <td>SSF Number</td>
                <td><?php echo $payslips[0]['Vwpayslip']['SSF_No']; ?></td>
            </tr>
            <tr>
                <td>Location</td>
                <td><?php echo $payslips[0]['Vwpayslip']['Location']; ?></td>
            </tr>
                  <tr>
                    <td>Month</td>
                    <td style="font-weight:bold;"><?php echo   date('M Y', strtotime($payslips[0]['Vwpayslip']['PayPeriod'])); ?></td>
                </tr>
        </tbody></table>
</div>
<div><h3>Summary</h3></div>

<table class="summary_table" style="width:100%">
<tbody>
<tr>
<td>            <table class="summary_table" style="width=50%;">        
                <tbody>
<tr class="higher_order"><td>Earnings</td><td></td><td></td><td>Amount</td></tr>
                    <tr><td>Basic</td><td></td><td></td><td><?php echo number_format($payslips[0]['Vwpayslip']['Basic'],2); ?></td></tr>
                    <?php
                    $total_earnings = 0.0;
                    $total_deductions = 0.0;
                    $total_earnings = $total_earnings + $payslips[0]['Vwpayslip']['Basic'];
//echo "TOTAL EARNINGS  ".$total_earnings;
                    $total_deductions = $total_deductions + $payslips[0]['Vwpayslip']['Tax'] + $payslips[0]['Vwpayslip']['SSF_Deduction'];
//echo "TOTAL DEDUCTIONS".$total_deductions;

                    foreach ($payslips as $val) {
                        if ($val['Vwpayslip']['Type'] == "ALLOW" && $val['Vwpayslip']['Amount'] != "0") {
                            $total_earnings = $total_earnings + $val['Vwpayslip']['Amount'];
                            ?>
                            <tr>
                                <td><?php echo $val['Vwpayslip']['Description']; ?></td>
                                <td><?php echo number_format($val['Vwpayslip']['Amount'],2); ?></td>
                            </tr> 

                            <?php
                        }
                    }
                    ?>

                </tbody></table></td>

<td>
<table class="summary_table" style="width="50%;">
            <tbody>
            <tr class="higher_order">
            <td>Deductions</td>
            <td>Principle</td>
            <td>Refunded</td>
            <td>Amount</td>
            <td>Balance</td>
            </tr>
                <tr><td>Social Security</td><td></td><td></td><td><?php echo number_format($payslips[0]['Vwpayslip']['SSF_Deduction'],2); ?></td><td></td></tr>
                <tr><td>Income Tax</td><td></td><td></td><td><?php echo number_format($payslips[0]['Vwpayslip']['Tax'],2);; ?></td><td></td> </tr>
                <?php
                foreach ($payslips as $val) {
                    if (
                            ($val['Vwpayslip']['Type'] == "LOAN" && $val['Vwpayslip']['LoanGranted'] != "0") ||
                            ($val['Vwpayslip']['Type'] == "FUND" && $val['Vwpayslip']['Amount'] != "0") ||
                            ($val['Vwpayslip']['Type'] == "DEDUCTION" && $val['Vwpayslip']['Amount'] != "0")
                    ) {
                        ?>
                        <tr>
                            <td><?php {
                    echo $val['Vwpayslip']['Description'];
                }
                        ?></td>                        
                            <td><?php
                        if ($val['Vwpayslip']['Type'] == "LOAN" && $val['Vwpayslip']['LoanGranted'] != "0") {
                            echo number_format($val['Vwpayslip']['LoanGranted'],2);
                        }
                        ?></td>
                            <td><!--Refunded--></td>
                            <td>
                                <?php
                                if ($val['Vwpayslip']['Type'] == "LOAN" && $val['Vwpayslip']['LoanGranted'] != "0") {
                                    echo number_format($val['Vwpayslip']['LoanDeduction'],2);
                                    $total_deductions = $total_deductions + $val['Vwpayslip']['LoanDeduction'];
                                }
                                ?>
                                <?php
                                if ($val['Vwpayslip']['Type'] == "FUND" && $val['Vwpayslip']['Amount'] != "0") {
                                    echo number_format($val['Vwpayslip']['Amount'],2);
                                    $total_deductions = $total_deductions + $val['Vwpayslip']['Amount'];
                                }
                                ?>
                                <?php
                                if ($val['Vwpayslip']['Type'] == "DEDUCTION" && $val['Vwpayslip']['Amount'] != "0") {
                                    echo number_format($val['Vwpayslip']['Amount'],2);
                                    $total_deductions = $total_deductions + $val['Vwpayslip']['Amount'];
                                }
                                ?>

                            </td>    
                            <td><?php
                        if ($val['Vwpayslip']['Type'] == "LOAN" && $val['Vwpayslip']['LoanGranted'] != "0") {
                            echo number_format($val['Vwpayslip']['Remaining'],2);
                        }
                                ?></td>
                        </tr> 
                        <?php
                    }
                }
                ?>

            </tbody>
</table>
</td></tr>


<tr>
 <td style="width:50%;"><span style="font-weight:bolder;">TOTAL EARNING</span><span style="margin-left:80px;"><?php echo number_format($total_earnings, 2); ?></span></td>
 <td style="text-align:left;"><span style="font-weight:bolder;">TOTAL DEDUCTIONS</span><span style="margin-left:100px;"><?php echo number_format($total_deductions, 2); ?></span></td>      
</tr>
 <tr>
 <td style="width:50%;"><span >EMPLOYER SSF</span><span style="margin-left:85px;"><?php echo number_format($payslips[0]['Vwpayslip']['Employers_SSF'], 2); ?></span></td>
 <td style="text-align:left;"><span style="font-weight:bolder;">NET PAY</span><span style="margin-left:163px;"><?php echo number_format($total_earnings - $total_deductions, 2); ?></span></td>
 </tr>
</table>
  