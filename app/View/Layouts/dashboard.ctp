<!DOCTYPE html>


<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Easy HR Payroll - Login</title>

        <!-- Maniac stylesheets -->
        <?php
        echo $this->Html->css('css/bootstrap.min.css');
        echo $this->Html->css('css/font-awesome.min.css');
        echo $this->Html->css('css/animate/animate.min.css');
        echo $this->Html->css('css/bootstrapValidator/bootstrapValidator.min.css');
        echo $this->Html->css('css/iCheck/all.css');
        echo $this->Html->css('css/style.css');
        ?>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
                        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="login fixed">
        <div class="wrapper animated flipInY">
            <div class="logo">
			<img style="width:200px;height:100px;"  src="<?php echo $this->webroot; ?>img/thor.jpg"/> 
			<!--<a href="#"><i class="fa fa-bolt"></i>Easy HR Payroll</a>-->
			</div>
            <div class="box">
                <div class="header clearfix">
                    <div class="pull-left"><i class="fa fa-sign-in"></i> Sign In</div>
                    <div class="pull-right"><a href="#"><!--<i class="fa fa-times"></i>--></a></div>
                </div>
                <?php echo $content_for_layout; ?>

            </div>
            <footer>
                Copyright &copy; <?php echo date('Y'); ?>


                <ul class="list-unstyled list-inline">
                    <li></li>
                </ul>
            </footer>
        </div>

        <!-- Javascript -->


        <?php
        // Javascript
        echo $this->Html->script('js/plugins/jquery/jquery-1.10.2.min.js');
        echo $this->Html->script('js/plugins/jquery-ui/jquery-ui-1.10.4.min.js');
        //bootstrap
        echo $this->Html->script('js/plugins/bootstrap/bootstrap.min.js');
        //interface
        echo $this->Html->script('js/plugins/pace/pace.min.js');
        //forms
        echo $this->Html->script('js/plugins/bootstrapValidator/bootstrapValidator.min.js');
        echo $this->Html->script('js/plugins/iCheck/icheck.min.js');
        ?>

        <script type="text/javascript">
            //iCheck
            $("input[type='checkbox'], input[type='radio']").iCheck({
                checkboxClass: 'icheckbox_minimal',
                radioClass: 'iradio_minimal'
            });
			
            $(document).ready(function() {
                $('#loginform').bootstrapValidator({
                    message: 'This value is not valid',
                    fields: {
                        username: {
                            message: 'The username is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The username is required and can\'t be empty'
                                }
                            }
                        },
                        password: {
                            validators: {
                                notEmpty: {
                                    message: 'The password is required and can\'t be empty'
                                }
                            }
                        }
                    }
                });
            });
        </script>
    </body>
</html>
