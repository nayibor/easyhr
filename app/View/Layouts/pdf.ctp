<!DOCTYPE html>
<html>
    <head>
        <!-- Meta -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8;" />
        <!-- End of Meta -->

        <!-- Page title -->
        <title>Easy Hr - <?php echo $title_for_layout; ?></title>
        <!-- End of Page title -->

    </head>

    <body>
        <?php echo $content_for_layout; ?>
    </body>
</html>