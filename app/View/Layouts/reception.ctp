<?php
//$member_data = $_SESSION['memberData'];
//print_r($member_data);
?>






<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">

                <title>Easy HR PAYROLL</title>

                <!-- EasyHr payroll stylesheets -->
                <?php
                echo $this->Html->css('css/bootstrap.min.css');
                echo $this->Html->css('css/font-awesome.min.css');
                echo $this->Html->css('css/animate/animate.min.css');
                echo $this->Html->css('css/datatables/dataTables.bootstrap.css');
                echo $this->Html->css('css/style.css');
                echo $this->Html->css('css/jquery-ui.css');
                ?>

                <!--HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries-->
                <!--[if lt IE 9]>
                <script src = "https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
                <![endif]-->
                <style>

                    #password_diag form{
                        margin-top: 40px;
                    }
                    #password_diag form label {
                        width: 150px;
                        margin-right: 10px;
                    }   
                    #message_info{text-align:center;margin-top:10px;font-family: Tahoma, Verdana, Arial, sans-serif !important;
                    }
                    .ui-dialog .ui-dialog-titlebar-close{display: none;}
                </style>
                </head>
                <body class="fixed">
                    <input type="hidden" name="password_save_url" id="password_save_url" value="<?php echo $this->Html->url(array('controller' => 'receptions', 'action' => 'change_password')); ?>" />
                    <input type="hidden" name="logout_url" id="logout_url" value="<?php echo $this->Html->url(array('controller' => 'dashboards', 'action' => 'logoutUser')); ?>" />

                    <!-- Header -->
                    <header>
                        <a href="#" class="logo"><i class="fa fa-bolt"></i> <span></span></a>
                        <nav class="navbar navbar-static-top">

                            <a href="#" class="navbar-btn sidebar-toggle">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </a>

                            <div class="navbar-header">

                            </div>
                            <div class="navbar-right">
                                <ul class="nav navbar-nav">


                                    <li class="dropdown widget-user">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <!--<img src="img/avatar.jpg" class="pull-left" alt="image" />-->
                                            <span>Settings <?php ////echo "hello " . $member_data['firstname'] . "  "                                                                   ?><i class="fa fa-caret-down"></i></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a name="open_pass" id="open_pass" href="#"><i class="fa fa-cog"></i>Password</a>
                                            </li>
                                            <li class="footer">
                                                <a href="<?php echo $this->Html->url(array('controller' => 'dashboards', 'action' => 'logoutUser')); ?>"><i class="fa fa-power-off"></i>Logout</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </header>
                    <!-- /.header -->

                    <!-- wrapper -->
                    <div class="wrapper">


                        <div class="leftside">


                        </div>

                        <div class="rightside">
                            <div id="password_diag" name="password_diag" title="Basic dialog">
                                <form >
                                    <fieldset>
                                     
                                        <div>
                                            <label>Old Password</label>
                                            <input type="password" maxlength="20" placeholder="Enter Old Password" required  id="old_password" name="old_password"  class="pass text ui-widget-content ui-corner-all" />
                                        </div> 
                                        
                                        <div>
                                            <label>New Password</label>
                                            <input type="password" maxlength="20" placeholder="Enter New Password" required  id="password_new" name="password_new"  class=" pass text ui-widget-content ui-corner-all" />
                                        </div>  
                                        <div>
                                            <label>Repeat Password</label>
                                            <input type="password" maxlength="20" placeholder="Repeat New Password" required  name="repeat_password" id="repeat_password" class=" pass text ui-widget-content ui-corner-all"/>
                                        </div>      
                                        <div>
                                            <label style="width:100% !important;"  id="message_info"><i class="fa fa-info-circle"></i></label>
                                        </div>   
                                        <!-- Allow form submission with keyboard without duplicating the dialog button -->
                                    </fieldset>
                                </form>

                            </div>
                            <?php echo $content_for_layout; ?>

                        </div><!-- /.wrapper -->
                    </div>
                    <?php
//this is for the js stuff
// Javascript
                    echo $this->Html->script('js/plugins/jquery/jquery-1.10.2.min.js');
                    echo $this->Html->script('js/plugins/jquery-ui/jquery-ui-1.10.4.min.js');
//bootstrap

                    echo $this->Html->script('js/reset_password.js');
                    echo $this->Html->script('js/view_payroll.js');


                    echo $this->Html->script('js/plugins/bootstrap/bootstrap.min.js');
//interface
                    echo $this->Html->script('js/plugins/slimScroll/jquery.slimscroll.min.js');
//forms
                    // echo $this->Html->script('js/plugins/datepicker/datepicker.js');
                    echo $this->Html->script('js/plugins/monthpicker/jquery.mtz.monthpicker.js');

//   echo $this->Html->script('js/plugins/datatables/dataTables.bootstrap.js');

                    echo $this->Html->script('js/custom.js');
                    ?>                  

                </body>
                </html>
