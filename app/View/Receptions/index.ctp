<style type="text/css">


    #request_details h3 {
        font-size: 12px;
        font-weight: bold;
        border-bottom: dotted #ccc 1px;
        padding-bottom: 3px;
        color: #222;
        float: left;
    }
    #request_details img {
        float:right;
    }

    #request_details .schedule {
        padding: 4px;
        margin: 3px 0px;
        border-radius: 4px;
        /*width: 560px;*/
    }

    #request_details .schedule.expanded {
        border: solid #ddd 1px;
        border-radius: 5px;
    }

    #request_details .schedule h4.divider {
        font-size: 11px;
        font-family: "Tahoma", sans-serif;
        color: #333;
        position: relative;
    }

    #request_details .schedule h4.divider .expander {
        background-image: url(../../img/expand_3.png);
        background-position: top;
        background-repeat: no-repeat;
        padding: 9px;
        margin-right: 5px;
        text-decoration: none;
        display: inline-block;
        vertical-align: top;
    }

    #request_details .schedule h4.divider .expander.collapsed {
        background-image: url(../../img/expand_3.png);
    }

    #request_details .schedule h4.divider .expander.expanded {
        background-image: url(../../img/collapse_3.png);
    }

    #request_details .schedule h4.divider .dates {
        display: inline-block;
        vertical-align: top;
        width: 380px;
        line-height: 16px;
        font-family: Tahoma, Geneva, sans-serif;
        font-size: 11px;
        text-transform: uppercase;
    }

    #request_details .schedule h4.divider .options {
        width: 140px;
        top: 0px;
        right: 0px;
        display: block;
        position: absolute;
        text-align: right;
        padding: 3px;
    }

    #request_details .schedule h4.divider .options a {
        color: #069;
        margin-right: 10px;
        text-decoration: none;
    }

    #request_details .schedule .services {
        padding: 10px;
        display: none;
    }

    #request_details .schedule .services td {
        font-size: 11px;
        padding: 5px 10px;
    }

    #request_details .schedule .services td ul {
        list-style-type: square;
        padding-left: 15px;
        margin-bottom: 15px;
    }

    #request_details .schedule .services td ul li {
        padding: 3px;
    }

    #request_details #attachment_link_list a {
        text-decoration: none;
        color: #069;
    }

    #request_details #attachment_link_list a:hover {
        text-decoration: none;
        color: #096;
    }

    #request_details #summary_table td {
        padding: 6px;
        font-size: 11px;
    }

    #request_details #summary_table td:first-child {
        font-weight: bold;
        width: 30%;
    }

    #request_details .menu_button {
        padding-right: 25px;
        background-image: url('../../css/core/menu_down.png');
        background-position: 95%;
        background-repeat: no-repeat;
        text-decoration: none;
        font-size: 11px;
        border: none;
        background-color: transparent;
        display: inline-block;
    }

    #request_details .menu_button .menu {
        position: absolute;
        border: solid #eee 1px;
        top: 27px;
        left: -100px;
        background-color: #f9f9f9;
        display: none;
        box-shadow: 1px 1px 3px #ccc;
    }

    #request_details .menu_button .menu li {
        padding: 5px 10px;
        min-width: 130px;
        max-width: 180px;
        width: auto;
        text-align: left;
        font-size: 11px;
    }

    #request_details .menu_button .menu li:hover {
        background-color: #fcfcfc;
    }

    #request_details .menu_button .menu li:not(:last-child) {
        border-bottom: solid #eee 1px;  }
    .pending .dates {color: black;}
    .arrived .dates {color: #cc6600;}
    .departed .dates {color: green; }
    .cancelled .dates {color: red; }


    .master_block{
        width:40%;
        display: table-cell;
        width: 60%;
        vertical-align: top;
        margin: 0;
        padding: 0;
        font-size: 13px;
        border-spacing: 4px;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }


    .details_block{
        width:60%;
        display: table-cell;
        padding: 5px;
        padding-left: 10px;
        border-left: dotted #ddd 1px;
        vertical-align: top;
        margin: 0;
        font-size: 13px;
        border-spacing: 4px;
        font-family: Tahoma, Verdana, Arial, sans-serif;

    }

    #payroll_list_table{
        margin-top:40px;}

    #table_div i{
        text-align:center !important;
    }
    #summary_table thead{display: table-header-group;
                         vertical-align: middle;
                         border-color: inherit;
                         font-weight: bolder;}
    #summary_table .net_data{ font-weight: bolder;}
     #summary_table tr td{white-space: nowrap !important;}
   
/**#payroll_list_table tr:hover{background-color:#ffffeb !important;}**/

 td.mtz-monthpicker-month {
        text-align:center;
      }
      select.mtz-monthpicker:focus {
        outline: none;
      }
</style>







<input type="hidden" name="payroll_list_url" id="payroll_list_url" value="<?php echo $this->Html->url(array('controller' => 'receptions', 'action' => 'payroll_list')); ?>" />
<input type="hidden" name="payroll_slip_url" id="payroll_slip_url" value="<?php echo $this->Html->url(array('controller' => 'receptions', 'action' => 'view_payslip')); ?>" />
<input type="hidden" name="payroll_pdf_url" id="payroll_pdf_url" value="<?php echo $this->Html->url(array('controller' => 'receptions', 'action' => 'download_payslip')); ?>" />

<div class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12"><!-- /.box -->

            <div class="box">
                <div class="box-title">
                    <h3>Payroll</h3>
                </div><!-- /.box-title -->
                <div class="box-body">

                    <div class="master_block" style="width:30%;height:700px;">
                        <form>
                            <div class="input-group">
                                <input type="text" name="from_date_range" id="from_date_range" class="form-control date-picker" placeholder="From...">
                                <input type="text" name="to_date_range" id="to_date_range"  class="form-control " placeholder="To...">

                                <span class="input-group-btn">
                                    <button type="submit" name="payroll_btn" id="payroll_btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>
                        <div id="table_div">
                        </div>

                    </div>


                    <div class="details_block" style="width:700px;">

                        <div class="details_options">
                            <h3 style="font-weight: bold; font-size: 12px; color: #222;"></h3>
                        </div>

                        <div class="details_pane" id="services_show" style="padding: 10px;">
                     </div>
                    </div>              
                </div>
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</div>

