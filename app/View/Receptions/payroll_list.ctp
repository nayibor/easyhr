<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<table id="payroll_list_table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Month/Year</th>
            <th>View</th>
            <th>Download</th>

        </tr>
    </thead>
    <tbody>

        <?php foreach ($slips as $val) { ?>
            <tr payperiod="<?php echo $val['Vwpayslip']['PayPeriod']; ?>">

                <td>  <?php echo date('M Y', strtotime($val['Vwpayslip']['PayPeriod'])); ?>  </td>
                <td><a href='#' class="logo payroll_view"  style="text-align:center"><i class="fa fa-info"></i></a></td>
                <td><a href="#" class="logo payroll_pdf"><i class="fa fa-file-pdf-o"></i></a></td>
            </tr>
        <?php } ?>
    </tbody>
</table>


<div class="row" style="text-align: center;margin: 0px !important;">
    <ul class="pagination">
        <?php
       

        echo " <li>" . $this->Paginator->first('< first ', array('class' => 'pglink'), null, array('class' => 'pglink')) . "</li>";
        echo " <li>" . $this->Paginator->prev('<< previous ', array('class' => 'pglink'), null, array('class' => 'pglink')) . "</li>";
        echo " <li>" . $this->Paginator->next('next >> ', array('class' => 'pglink'), null, array('class' => 'pglink')) . "</li>";
        echo " <li>" . $this->Paginator->last('last > ', array('class' => 'pglink'), null, array('class' => 'pglink')) . "</li>";
     
        ?>
    </ul>
</div>
<?php
//debug($this->Paginator->params()); ?>