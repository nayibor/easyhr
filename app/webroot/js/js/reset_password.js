/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var password_js={
    
    
    
    check_password:function(){
        var _this=this,check_pass_status="true";
        
		
		   $(".pass").each(function(){
      
            if(!(document.getElementById($(this).attr("id")).checkValidity()) || $(this).val()=="" ){
				  $(this).addClass( "ui-state-error" );
                check_pass_status="false";
            }else
            {
				if($(this).attr("id")=="old_password"){	
				$(this).removeClass( "ui-state-error" );
				}
				
            }
        });
		

  
		
        if($("#password_new").val()!=$("#repeat_password").val() || ($("#repeat_password").val() =="" || $("#password_new").val()=="") )
            {
            $("#repeat_password,#password_new").addClass( "ui-state-error" );
            check_pass_status="false";
        }else{
			 $("#repeat_password,#password_new").removeClass( "ui-state-error" );	
		}
		
		      if(check_pass_status=="false")
				{return ;}
		
		
        if(check_pass_status=="true")
        {
            _this.update_password();
        }
        
    },
    
    update_password:function(){
        
        url=$("#password_save_url").val();
        pass_data="pass_data="+$("#password_new").val()+"&password_old="+ $("#old_password").val();
        $.ajax({
            url: url,
            dataType:'json',
            type: 'POST',
            data:pass_data,
            beforeSend:function(){
                $(":button:contains('Change Password')").prop("disabled",true).addClass("ui-state-disabled");
                $("#message_info").show();
                $("#message_info i").html(" Changing Password ...");
              
            },
            error:function(data) {
                $(":button:contains('Change Password')").prop("disabled",false).removeClass("ui-state-disabled");
                $("#message_info i").html(" Error.Please Try Again");
                $("#message_info").css("display","visible");            //  console.log(data);
            //  alert("data has been loaded");
            
            },
            success:function(data) {
                //  console.log(data);
                $(":button:contains('Change Password')").prop("disabled",false).removeClass("ui-state-disabled");
                if(data.status=="true" ){
                    $("#message_info i").html(" Password Change Successful.");
                    $("#message_info").css("display","visible");
                   /*  window.location.href=$("#logout_url").val();
                    **/
                }
                else if (data.status=="false" ){
                    $("#message_info i").html(data.msg);
                    //$("#repeat_password,#password_new").addClass( "ui-state-error" );

                }
            //  alert("data has been loaded");
            //   window.location.hr=
            },
            error:function(data){
              $("#message_info i").html("There Was An Error .Please Try Again");
            }
        })
    },
   
    
    init:function(){
        var _this=this;
        $("#password_diag").hide();
        $("#message_info").hide();

        $( "#password_diag" ).dialog({
            title:"Change Password",
            autoOpen: false,
            height: 300,
            width: 500,
            position:"center",
            modal: true,
            buttons: {
                "Change Password": function() {
                    _this.check_password();
                },
                "Close": function() {
                    $("#password_diag form")[0].reset(); 
                    $("#repeat_password,#password_new,#old_password").removeClass( "ui-state-error" );
                    $("#message_info").hide();
                    $("#message_info i").html("");
                    $( this ).dialog( "close" );
                }
               
            }
          
        });
        
        
        
        $( "#open_pass" ).click(function() {
            //  $('div.ui-dialog-buttonpane').show();
            $( "#password_diag" ).dialog( "open" );
        });
    }  
  
}


$(document).ready(function($) {

    password_js.init();
})
