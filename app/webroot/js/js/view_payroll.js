/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var payroll={
    
    load_url:$("#payroll_list_url").val(),
    payslip_url:$("#payroll_slip_url").val(),
    paypdf:$("#payroll_pdf_url").val(),
    
    load_payroll:function(page_link){
        _this=this;
        
        var val = $("#from_date_range").val();
        var sval= $("#to_date_range").val();
        
        filter=(val!="")? "filter="+"01/"+val :"filter=null";
        sfilter=(sval!="")? "sfilter="+_this.format_end($("#to_date_range").val())+"/"+sval: "sfilter=null";
        
        get_filter =filter+"&"+sfilter;
        $.ajax({
            url: page_link,
            dataType:'html',
            data:get_filter,
            beforeSend:function(){
                $("#payroll_btn i").addClass("fa fa-spinner");
                $("#table_div").remove("table");
                
            },
            error:function(data) {
                //  console.log(data);
                //  alert("data has been loaded");
                $("#payroll_btn i").removeClass("fa fa-spinner");
                $("#payroll_btn i").addClass("fa fa-search");
                $("#table_div").html(data);
            },
            success:function(data) {
                //  console.log(data);
                //  alert("data has been loaded");
                $("#payroll_btn i").removeClass("fa fa-spinner");
                $("#payroll_btn i").addClass("fa fa-search");
                $("#table_div").html(data);
            },
            error:function(data){
          
            }
        })
    },
    format_end:function(date_input){
        _this=this;
        var date_parts=date_input.split("/");        
        var  days_month=function(month,year){
            return new Date(year,month,0).getDate();
        }
        return(days_month(date_parts[0],date_parts[1]));
    ;
    },
    load_payslip:function(payperiod,type){
        var _this=this;
        
        get_filter ="payperiod="+payperiod; 
        var url="";
        var iframe_payslip;
       
        if(type=="view")
        {
            url=_this.payslip_url;
            $.ajax({
                url:url,
                dataType:'html',
                data:get_filter,
                beforeSend:function(){
                    $(".details_pane").html("<i fa fa-spinner></i>");
                
                },
                error:function(data) {
                    //  console.log(data);
                    //  alert("data has been loaded");
               
                    $(".details_pane").html("");
                },
                success:function(data) {
                    //  console.log(data);
                    //  alert("data has been loaded");
                    if(type=="view")
                    {
                        $(".details_pane").html(data);
                    }
                    else if(type=="pdf"){
                  
                    }
                },
                error:function(data){
          
                }
            })
        }
        
        else if(type=="pdf"){
            url=_this.paypdf+"?"+get_filter;
            iframe_payslip=document.createElement("iframe");
            $(iframe_payslip).attr("src",url);
            $(iframe_payslip).css("display","none");
            $("body").append(iframe_payslip);
        };
        

    /** **/
    },
    
    init:function(){
        var _this=this;
        $(".navbar-btn").hide();
        $(".navbar-btn").click();
        $(".navbar-btn").unbind('click');

        _this.load_payroll(_this.load_url);

        $( "#from_date_range" ).monthpicker();
        $( "#to_date_range" ).monthpicker();
        
        $(document).on("click",".fa-info",function(e){
            e.preventDefault();
            _this.load_payslip($(this).closest("tr").attr("payperiod"),"view");
            
        });
       
        $(document).on("click","a.payroll_pdf",function(e){
            e.preventDefault();
            _this.load_payslip($(this).closest("tr").attr("payperiod"),"pdf");
            
        });
        
        $(document).on("click","span.pglink a",function(e){
            e.preventDefault();
            _this.load_payroll($(this).attr('href'));  
        });
        
        
        $("#payroll_btn").on("click",function(e) {
            e.preventDefault();
            _this.load_payroll(_this.load_url);

           
        });
        
    }
}

$(document).ready(function($) {

    payroll.init();
})




